﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Text
Imports System.Threading

Public Class ConnectForm
    Public ClientSocket As TcpClient
    Public ServerStream As NetworkStream

    Private Sub Connect()
        ClientSocket = New TcpClient()
        Try
            ClientSocket.Connect(IPAddress.Parse(txtIP.Text), 8888)
            ServerStream = ClientSocket.GetStream()
            Dim connectionInfo As New List(Of String)
            connectionInfo.Add(txtName.Text)
            connectionInfo.Add(Environment.UserName)
            Dim bytes = System.Text.Encoding.UTF8.GetBytes(txtSentence.Text)
            connectionInfo.Add(Convert.ToBase64String(bytes))
            ' Easy addition in furture versions, yay..
            Dim connString As String = String.Join(";", connectionInfo)
            Send(connString)
            Threading.Thread.Sleep(3)
            Send("Ignore")
            Send("Ignore")
            Send("Ignore")
            Dim t As New Thread(AddressOf getMessage)
            t.Start()
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
    End Sub

    Private Sub Send(msg As String)
        If String.IsNullOrWhiteSpace(msg) Then Return
        Dim bytes = System.Text.Encoding.UTF8.GetBytes(msg)
        msg = Convert.ToBase64String(bytes)
        Try
            msg = msg + "%"
            Dim outStream = System.Text.Encoding.UTF8.GetBytes(msg)
            ServerStream.Write(outStream, 0, outStream.Length)
            ServerStream.Flush()
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
    End Sub

    Private Sub HandleMsg(message As String)
        MsgBox(message)
    End Sub

    Private Sub getMessage()
        Try
            While True
                Dim nextHandleData As String = ""
                Dim bytesFrom(1) As Byte
                Do
                    ServerStream.Read(bytesFrom, 0, 1)
                    nextHandleData += Encoding.UTF8.GetString(bytesFrom)
                    bytesFrom = New Byte() {""}
                Loop Until nextHandleData.Contains("%")
                nextHandleData = nextHandleData.Remove(nextHandleData.Length - 1)
                Try
                    Dim bytes = Convert.FromBase64String(nextHandleData)
                    nextHandleData = Encoding.UTF8.GetString(bytes)
                    Dim t As New Thread(Sub() HandleMsg(nextHandleData))
                    t.Start()
                Catch ex As Exception
                    MsgBox(ex.ToString())
                End Try
            End While
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        If String.IsNullOrWhiteSpace(txtIP.Text + txtName.Text + txtSentence.Text) Then
            MsgBox("You have not filled in all of the text boxes, please do so.")
        End If
        If IPAddress.TryParse(txtIP.Text, IPAddress.Parse("127.1")) = False Then
            MsgBox("The IP you entered is invalid.")
        End If
        Connect()
    End Sub

    Private Sub lblSentence_MouseEnter(sender As Object, e As EventArgs) Handles lblSentence.MouseEnter
        If String.IsNullOrWhiteSpace(txtSentence.Text) Then Return
        sender.Text = txtSentence.Text
    End Sub

    Private Sub lblSentence_MouseLeave(sender As Object, e As EventArgs) Handles lblSentence.MouseLeave
        sender.Text = "Your sentence will appear here, when you hover over it."
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Send("I be testing, yaaarr!")
    End Sub
End Class

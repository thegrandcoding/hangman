﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConnectForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSentence = New System.Windows.Forms.TextBox()
        Me.lblSentence = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtIP = New System.Windows.Forms.TextBox()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Your name!"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Server IP!"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 137)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(221, 25)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "I want people to guess..."
        '
        'txtSentence
        '
        Me.txtSentence.Location = New System.Drawing.Point(18, 178)
        Me.txtSentence.MaxLength = 20
        Me.txtSentence.Name = "txtSentence"
        Me.txtSentence.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSentence.Size = New System.Drawing.Size(309, 22)
        Me.txtSentence.TabIndex = 3
        '
        'lblSentence
        '
        Me.lblSentence.AutoSize = True
        Me.lblSentence.Location = New System.Drawing.Point(333, 181)
        Me.lblSentence.Name = "lblSentence"
        Me.lblSentence.Size = New System.Drawing.Size(359, 17)
        Me.lblSentence.TabIndex = 4
        Me.lblSentence.Text = "Your sentence will appear here, when you hover over it."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 234)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(697, 34)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Above:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If you are randomly picked by the server, then the above sentence is what" &
    " everyone would be trying to guess." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(99, 10)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(135, 22)
        Me.txtName.TabIndex = 6
        '
        'txtIP
        '
        Me.txtIP.Location = New System.Drawing.Point(99, 50)
        Me.txtIP.Name = "txtIP"
        Me.txtIP.Size = New System.Drawing.Size(135, 22)
        Me.txtIP.TabIndex = 7
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(336, 10)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(366, 62)
        Me.btnConnect.TabIndex = 8
        Me.btnConnect.Text = "Connect!"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(403, 100)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(366, 62)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Connect!"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ConnectForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 287)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.txtIP)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblSentence)
        Me.Controls.Add(Me.txtSentence)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "ConnectForm"
        Me.Text = "Connect!"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtSentence As TextBox
    Friend WithEvents lblSentence As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtIP As TextBox
    Friend WithEvents btnConnect As Button
    Friend WithEvents Button1 As Button
End Class

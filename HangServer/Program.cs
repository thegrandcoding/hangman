﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Threading;

namespace Hangman
{
    public class Program
    {
        public static TcpListener ServerSocket;
        public static Dictionary<string, User> Users = new Dictionary<string, User>();
        public static void WriteToConsole(string message)
        {
            Console.WriteLine(DateTime.Now.ToString("[ddmmyy-H:mm:ss]") + " " + message);
        }

        public static IPAddress GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        static void Main(string[] args)
        {
            new Program().Start();
        }

        public void Start()
        {
            //WriteToConsole("Hey, lol!");
            ServerSocket = new TcpListener(GetLocalIPAddress(), 8888);
            ServerSocket.Start();
            WriteToConsole("Server listening at " + GetLocalIPAddress() + ":8888");
            Thread t = new Thread(NewClientHandler);
            t.Start();
            while(true)
            {
                Console.ReadLine();
            }
        }

        public void NewClientHandler()
        {
            try
            {
                TcpClient clientSocket;
                WriteToConsole("Starting client handler");
                while(true)
                {
                    clientSocket = ServerSocket.AcceptTcpClient();
                    NetworkStream netStream = clientSocket.GetStream();
                    while (true)
                    {
                        string nextHandleData = "";
                        Byte[] bytesFrom = new byte[1];
                        do
                        {
                            netStream.Read(bytesFrom, 0, 1);
                            nextHandleData += Encoding.UTF8.GetString(bytesFrom);
                            bytesFrom = new byte[1];
                        } while (nextHandleData.Contains("%") == false);
                        nextHandleData = nextHandleData.Remove(nextHandleData.Length - 1);
                        byte[] bytes = Convert.FromBase64String(nextHandleData);
                        nextHandleData = Encoding.UTF8.GetString(bytes);
                        string[] connStrings = nextHandleData.Split(';');
                        byte[] sent = Convert.FromBase64String(connStrings[2]);
                        string sentence = Encoding.UTF8.GetString(sent);
                        User newU = new User(connStrings[0], connStrings[1], sentence, clientSocket);
                        Users[newU.DisplayName] = newU;
                        ClientHandle clHan = new ClientHandle();
                        clHan.start(clientSocket, newU);
                    }
                }
            } catch(Exception e)
            {
                WriteToConsole(e.ToString());
            }
        }
    }
}

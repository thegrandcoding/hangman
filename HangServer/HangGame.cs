﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    public class HangGame
    {
        private List<Char> shownDefault = new List<char>() { ',', '.', '"', '!', '?', '£' };
        // Characters that are not turned into underscores
        public User WordGiver;
        public readonly string Sentence;
        public string KnownSentence = "";

        public HangGame(User wg, string sen)
        {
            WordGiver = wg;
            Sentence = sen;
            foreach(Char c in Sentence)
            {
                if(shownDefault.Contains(c))
                {
                    KnownSentence += c.ToString();
                } else
                {
                    KnownSentence += "_";
                }
            }
        }
    }
}

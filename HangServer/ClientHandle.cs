﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using static Hangman.Program;

namespace Hangman
{
    public class ClientHandle
    {
        public TcpClient Client;
        public User ClientUser;
        public string ClientName => ClientUser.DisplayName;


        public void start(TcpClient inClientSocket, User user)
        {
            ClientUser = user;
            Client = inClientSocket;
            ClientUser.Client = Client;
            Thread ctThread = new Thread(doChat);
            ctThread.Name = ClientName + "_Chat";
            ctThread.Start();
            WriteToConsole("Started new client: " + ClientName);
            WriteToConsole($"{user.DisplayName} {user.ComputerName} {user.SentenceIfPicked}");
            ClientUser.Send("&Connection&");
        }

        private void handleMsg(string message)
        {
            WriteToConsole($"{ClientName}: {message}");
        }

        private void doChat()
        {
            try
            {
                while(true)
                {
                    NetworkStream netStream = Client.GetStream();
                    while(true)
                    {
                        string nextHandleData = "";
                        Byte[] bytesFrom = new byte[1];
                        do
                        {
                            netStream.Read(bytesFrom, 0, 1);
                            nextHandleData += Encoding.UTF8.GetString(bytesFrom);
                            bytesFrom = new byte[1];
                        } while (nextHandleData.Contains("%") == false || nextHandleData.Contains("\0"));
                        nextHandleData = nextHandleData.Remove(nextHandleData.Length - 1); // removes trailing %
                        try
                        {
                            byte[] bytes = Convert.FromBase64String(nextHandleData);
                            nextHandleData = Encoding.UTF8.GetString(bytes);
                            Thread handleThread = new Thread(() => handleMsg(nextHandleData));
                            handleThread.Start();
                        } catch (FormatException ex)
                        {
                            WriteToConsole("Format error on " + nextHandleData + "; " + ex.ToString());
                        }
                    }
                }
            } catch (Exception ex) when (ex is SocketException || ex is System.IO.IOException)
            {
                WriteToConsole("Erorr: user " + ClientName + " has left. (1)");
            }
            
            
        }
    }
}

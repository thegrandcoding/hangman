﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using static Hangman.Program;

namespace Hangman
{
    public class User
    {
        public readonly string ComputerName;
        public string DisplayName;
        public TcpClient Client;

        public readonly string SentenceIfPicked;

        public User(string name, string compName, string sentence, TcpClient client)
        {
            DisplayName = name;
            ComputerName = compName;
            SentenceIfPicked = sentence;
        }

        public void Send(string msg)
        {
            if(String.IsNullOrWhiteSpace(msg)) { return; }
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(msg);
            msg = Convert.ToBase64String(bytes);
            try
            {
                msg = msg + "%";
                byte[] outStream = System.Text.Encoding.UTF8.GetBytes(msg);
                NetworkStream str = Client.GetStream();
                str.Write(outStream, 0, outStream.Length);
                str.Flush();
            }
            catch(Exception ex)
            {
                WriteToConsole(ex.ToString());
            }
        }
    }
}
